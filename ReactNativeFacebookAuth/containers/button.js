import React from "react";
import { View, Button, TouchableOpacity } from "react-native";
import { Card } from "react-native-elements";

export default class buttonScreen extends React.Component {
  render() {
    return (
      <View>
        <Card>
          <TouchableOpacity
            style={{ textAlign: "center" }}
            onPress={() => this.props.navigation.navigate("Home")}
          >
            Add
          </TouchableOpacity>
        </Card>
      </View>
    );
  }
}
