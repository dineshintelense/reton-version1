import React, { Component } from "react";
import { View, Text, StyleSheet, TouchableOpacity } from "react-native";
import Products from "../components/Products";
import { products } from "../Data";
import { connect } from "react-redux";
import ShoppingCartIcon from "./ShoppingCartIcon";
import { FontAwesome } from "@expo/vector-icons";
import { Card } from "react-native-elements";
import StarRating from "../views/StarRating";
import { AntDesign } from "@expo/vector-icons";
import Like from "../views/like";

class ProductDetailsScreen extends Component {
  render() {
    const { navigation } = this.props;
    const img = navigation.getParam("img");
    const price = navigation.getParam("price");
    const name = navigation.getParam("name");
    const qty = navigation.getParam("qty");
    const pid = navigation.getParam("pid");
    console.log("name", name);
    console.log("qty", qty);
    return (
      <View style={styles.container}>
        <Card
          containerStyle={{
            elevation: 0,
            borderColor: "Purple",
            borderRadius: 30
          }}
        >
          <Text
            numberOfLines={1}
            style={[
              styles.centerElement,
              {
                color: "purple",
                fontSize: 20,
                alignItems: "center",
                textAlign: "center"
              }
            ]}
            h2
          >
            <Text style={{ paddingRight: 30 }}>
              <AntDesign
                name="left"
                size={24}
                color="Purple"
                justifyContent="left"
                onPress={() => this.props.navigation.navigate("Home")}
              />
            </Text>
            <Text
              style={{ paddingRight: 10, paddingLeft: 10, fontWeight: "bold" }}
            >
              Garden & Lawn
            </Text>

            <Text style={{ paddingRight: 10, paddingLeft: 10 }}>
              <Like />
            </Text>

            <ShoppingCartIcon />
          </Text>
          <br></br>
          <br></br>
          <Card
            image={{ uri: img }}
            containerStyle={{ elevation: 0, borderColor: "transparent" }}
          />
          <br></br>
          <br></br>
          <Text style={styles.name} h2>
            {name}
          </Text>
          <Text style={styles.price} h2>
            $ {price}/hr
          </Text>
          <Text style={styles.name}>
            <StarRating />
          </Text>
        </Card>
        <br></br>

        <TouchableOpacity
          style={styles.button}
          onPress={() => {
            this.props.addItemToCart({ name, price, img, qty, pid });
          }}
        >
          <Text style={styles.buttonText}>ADD TO CART</Text>
        </TouchableOpacity>

        <br></br>
      </View>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    addItemToCart: product =>
      dispatch({ type: "ADD_TO_CART", payload: product })
  };
};

export default connect(null, mapDispatchToProps)(ProductDetailsScreen);

const styles = StyleSheet.create({
  container: {
    justifyContent: "top",
    alignItems: "top",

    flex: 1,
    backgroundColor: "white"
  },
  name: {
    color: "purple",
    fontSize: 20,
    alignItems: "center",
    textAlign: "center"
  },
  name1: {
    color: "purple",
    fontSize: 20,
    alignItems: "center",
    textAlign: "center",

    justifyContent: "space-around"
  },
  buttonroundLeft: {
    paddingVertical: 15,
    paddingHorizontal: 15,
    backgroundColor: "purple",
    borderRadius: 400,
    justifyContent: "space-round",
    position: "absolute",
    height: 50,
    width: 50,
    top: 10,
    marginTop: 500,
    marginLeft: 230
  },
  buttonroundRight: {
    paddingVertical: 15,
    paddingHorizontal: 15,
    backgroundColor: "purple",
    borderRadius: 400,
    justifyContent: "space-round",
    position: "absolute",
    height: 50,
    width: 50,
    top: 10,
    marginTop: 500,
    marginLeft: 290,
    marginRight: 20
  },
  button: {
    marginTop: 500,

    width: 150,
    backgroundColor: "purple",
    justifyContent: "center",
    alignItems: "center",
    textAlign: "center",
    position: "absolute",
    alignSelf: "center",
    height: 50,
    top: 10,
    borderRadius: 30
  },
  buttonText: {
    color: "white",
    fontWeight: "bold",
    textTransform: "uppercase",
    fontSize: 15,
    textAlign: "center",
    justifyContent: "center",
    alignItems: "center"
  },

  price: {
    fontWeight: "bold",
    marginBottom: 10,
    alignItems: "center",
    textAlign: "center",
    fontSize: 30,
    color: "purple"
  },
  description: {
    fontSize: 10,
    color: "#c1c4cd"
  },
  details: {
    justifyContent: "top",
    alignItems: "top",
    borderRadius: 20,
    flex: 1,
    backgroundColor: "white",
    height: 500
  }
});
