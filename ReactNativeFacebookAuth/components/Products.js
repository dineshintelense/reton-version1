import React, { Component } from "react";
import { View, Text, StyleSheet, TouchableOpacity } from "react-native";
import { Card, Button } from "react-native-elements";
import { withNavigation } from "react-navigation";
import { FontAwesome5 } from "@expo/vector-icons";
import StarRating from "../views/StarRating";
import { MaterialIcons } from "@expo/vector-icons";
import Like from "../views/like";
import axios from "axios";
const products1 = [];
const products2 = [];


class Products extends Component {
  state = {

    loading: true
  }
  componentDidMount() {

    //alert("inside");
    axios.post("http://localhost:3000/product").then(resp => {
      //alert(resp);


      let responsedata = {
        data:

        {
          _id: '',
          C_ID: '',
          name: '',
          img: '',
          price: '',
          rent: '',
          loc: ''
        }


      }
      var dataconvert = [];
      dataconvert = JSON.stringify(resp);


      responsedata = JSON.parse(dataconvert);
      // show result
      //  alert(responsedata)
      var output = '';

      for (var i = 0; i < responsedata.data['data'].length; i++) {
        var bit = responsedata.data['data'][i];

        // alert(bit.name);
        products1.push(bit);
        let data = {
          data:

          {
            _id: '',
            C_ID: '',
            name: '',
            img: '',
            price: '',
            rent: '',
            loc: ''
          }



        }
        var dataconvert1 = [];
        dataconvert1 = JSON.stringify(products1[i]);
        data.data = JSON.parse(dataconvert1);
        products2.push(data.data);

      };
      this.setState({ loading: false })


    })
      .catch(err => {
        console.log(err);
      })


  }


  renderProducts = products2 => {
    console.log(products2);
    return products2.map((item, index) => {
      return (
        <Card key={index}>
          <Text style={{ marginBottom: 10, marginTop: 20 }} h4>
            {item.name}
          </Text>
          <Text style={styles.price} h4>
            $ {item.price}/hr
            <Text style={{ paddingLeft: 150, justifyContent: "space-around" }}>
              <StarRating />
            </Text>
          </Text>

          <TouchableOpacity
            onPress={() =>
              this.props.navigation.navigate("ProductDetails", {
                name: item.name,

                price: item.price,

                img: item.img,

                qty: item.qty,
                pid: item.Pid
              })
            }
          >
            <Card
              image={item.img}
              containerStyle={{ elevation: 0, borderColor: "transparent" }}
            />
          </TouchableOpacity>

          <Card style={{ backgroundColor: "blue" }}>
            <Text numberOfLines={1} style={{ color: "purple" }}>
              Rented : {item.rent}
              <FontAwesome5
                name="shopping-bag"
                size={20}
                color="purple"
                style={{ paddingLeft: 100 }}
              />
            </Text>
            <br></br>
            <Text numberOfLines={1} style={{ color: "green" }}>
              <MaterialIcons name="location-on" size={22} color="green" />
              {item.loc}
              <Text style={{ paddingLeft: 110 }}>
                <Like />
              </Text>
            </Text>
          </Card>
        </Card>
      );
    });
  };

  render() {
    if (this.state.loading) {
      return 'Loading...'
    }

    return (
      <View style={styles.container}>
        {this.renderProducts(products2)}
      </View>
    );
  }
}
export default withNavigation(Products);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
  },
  name: {
    color: "#5a647d",
    fontWeight: "bold",
    fontSize: 30
  },
  price: {
    fontWeight: "bold",
    marginBottom: 10
  },
  description: {
    fontSize: 10,
    color: "#c1c4cd"
  }
});
